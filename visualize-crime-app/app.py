# flask_web/app.py

from flask import Flask, request, jsonify, render_template
import pandas as pd
from datetime import datetime
import time
import json
################################################################
## Read in data
################################################################

crimes = pd.read_csv("./data/out/enriched_cta_crime_data.csv", index_col=0)
cta = pd.read_csv("./data/out/enriched_cta_data.csv", index_col=0)

# Preprocesing
crimes["date_obj"] = crimes["date"].map(lambda x: datetime.strptime(x, "%Y-%m-%dT%H:%M:%S"))
crimes["date_only"] = crimes["date_obj"].map(lambda x: x.date())
crimes['n_days'] = (crimes['date_only']-crimes['date_only'].min()).dt.days
crimes['hour'] = pd.to_datetime(crimes['date']).dt.hour
crimes['minutes'] = crimes['date_obj'].dt.hour*60 + crimes['date_obj'].dt.minute

first_day = crimes['date_only'].min()
last_day = crimes['date_only'].max()
stations = list(crimes['station_name'].unique())
# print(crimes.head())
# print(cta.head())
################################################################
## Flask app
################################################################

app = Flask(__name__, static_url_path='/static')

# @app.route('/')
# def hello_world():
#     return 'Hey, we have Flask in a Docker container!'

@app.route('/')
def root():
    return render_template('index.html')

@app.route('/initialize_autocomplete')
def init_autocomplete():
    response = jsonify(stations)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/get_first_day')
def get_first_day():
    # print(first_day)
    response = jsonify(str(first_day))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/get_last_day')
def get_last_day():
    # print(first_day)
    response = jsonify(str(last_day))
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/crimemap')
def crime_at_station():

    station = request.args.get('station')
    print(f'Request for {station} received')

    df = crimes[crimes['station_name']==station].sort_values('date')

    #Version 1 -- square matrix
    # df_output = pd.pivot_table(df, index='date_only', columns='hour', values='longitude', aggfunc='count')
    # df_output= df_output.fillna(0).reset_index()
    # df_output['date_only'] = df_output['date_only'].astype(str)
    # data = json.loads(df_output.to_json(orient='records'))
    # print(df_output.head())

    # Version 2 -- only days with entries
    # df_output = df.groupby(['n_days','hour'])['longitude'].count().reset_index()
    df_output = df.groupby(['n_days','minutes'])['longitude'].count().reset_index()

    # Reindex
    # idx = range(0, (last_day - first_day).days )
    # print(len(idx))
    # print(df_output.shape)
    # idx = pd.date_range(first_day, last_day)
    # print(idx[:5])
    # print(df_output.index[:5])

    # df_output = df_output.reindex(idx, fill_value=0)

    data = df_output.values.tolist()

    # Return response
    response = jsonify(data)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response #jsonify(data)

@app.route('/echonumber')
def echonumber():
    print('echonumber')
    number = request.args.get('number')
    return number

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)
