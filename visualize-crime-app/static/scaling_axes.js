

function add_x_axis(themap, axisid, groupname)
{
////////////////////////////////////////////////////////////
// Initial setup
////////////////////////////////////////////////////////////
var r_bound = themap.getBounds()["_northEast"]["lng"]/60;
var l_bound = themap.getBounds()["_southWest"]["lng"]/60;

// Create scale
var scale = d3.scaleLinear()
              .domain([l_bound, r_bound])
              .range([0, 300]);

// Add scales to axis
var x_axis = d3.axisBottom()
               .scale(scale)
               .tickValues(d3.range(0, 26, 2));

//Append group and insert axis
var svg = d3.select("#" + axisid) //eft_xaxis
svg.append("g")
   .attr("id", groupname)
   .call(x_axis);

// map1.on("zoomstart", function (e) { console.log("ZOOMSTART", e); });

////////////////////////////////////////////////////////////
// Zoom behavior
////////////////////////////////////////////////////////////
themap.on("zoomend", function (e) {
                var r_bound = themap.getBounds()["_northEast"]["lng"]/60;
                var l_bound = themap.getBounds()["_southWest"]["lng"]/60;
                console.log(l_bound, r_bound);

                // Create scale
                var scale1 = d3.scaleLinear()
                              .domain([l_bound, r_bound])
                              .range([0, 300]);

                // Add scales to axis
                var x_axis1 = d3.axisBottom()
                               .scale(scale1)
                               .tickValues(d3.range(0, 26, 2));

                var svg1 = d3.select("#" + groupname)
                // console.log(svg1)
                svg1.transition(100).call(x_axis1)
              });

      themap.on("move", function () {

        var r_bound_g = themap.getBounds()["_northEast"]["lng"]/60;
        var l_bound_g = themap.getBounds()["_southWest"]["lng"]/60;

        // Create scale
        var scale_generic = d3.scaleLinear()
                            .domain([l_bound_g, r_bound_g])
                            .range([0, 300]);


        // Add scales to axis
        var x_axis_generic = d3.axisBottom()
                       .scale(scale_generic)
                       .tickValues(d3.range(0, 26, 2));

        d3.select("#" + groupname).call( x_axis_generic );
      });
}






function add_y_axis(themap, axisid, groupname)
{
  ////////////////////////////////////////////////////////////
  // Initial setup
  ////////////////////////////////////////////////////////////
  var t_bound = themap.getBounds()["_northEast"]["lat"];
  var b_bound = themap.getBounds()["_southWest"]["lat"];

  console.log(t_bound);
  console.log(b_bound);

  // Create scale
  var f_date = new Date(first_day.split("-"));
  var l_date = new Date(first_day.split("-"));
  l_date.setDate(l_date.getDate() + t_bound);
  console.log(f_date);
  console.log(l_date);

  var scale = d3.scaleLinear()
                .domain([f_date, l_date])
                .range([800, 0]);


  // var dateArray = d3.time.scale()
  //         .domain([new Date(2013, 2, 28), new Date(2013, 3, 2)])
  //         .ticks(d3.time.days, 1);


  // Add scales to axis
  var y_axis = d3.axisRight()
                 .scale(scale)
                 .tickSize(10)
                 .tickFormat(d3.timeFormat("%Y-%m"));//subdivide 12 months in a year
                 // .tickValues(d3.range(0, 26, 2));

  //Append group and insert axis
  var svg = d3.select("#" + axisid)
  svg.append("g")
     .attr("id", groupname)
     .call(y_axis);

  // map1.on("zoomstart", function (e) { console.log("ZOOMSTART", e); });

  ////////////////////////////////////////////////////////////
  // Zoom behavior
  ////////////////////////////////////////////////////////////
  themap.on("zoomend", function (e) {
                  var t_bound = themap.getBounds()["_northEast"]["lat"];
                  var b_bound = themap.getBounds()["_southWest"]["lat"];
                  console.log(t_bound, b_bound);

                  // Create scale
                  var f_date = new Date(first_day.split("-"));
                  f_date.setDate(f_date.getDate() + b_bound);

                  var l_date = new Date(first_day.split("-"));
                  l_date.setDate(l_date.getDate() + t_bound);

                  console.log(f_date);
                  console.log(l_date);

                  // Create scale
                  var scale1 = d3.scaleLinear()
                                .domain([f_date, l_date])
                                .range([800, 0]);

                  // Add scales to axis
                  var y_axis1 = d3.axisRight()
                                 .scale(scale1)
                                 .tickSize(10)
                                 .tickFormat(d3.timeFormat("%Y-%m"));

                  var svg1 = d3.select("#" + groupname)
                  // console.log(svg1)
                  svg1.transition(100).call(y_axis1)
                });

      themap.on("move", function () {
        var t_bound = themap.getBounds()["_northEast"]["lat"];
        var b_bound = themap.getBounds()["_southWest"]["lat"];
        // console.log(t_bound, b_bound);

        // Create scale
        var f_date = new Date(first_day.split("-"));
        f_date.setDate(f_date.getDate() + b_bound);

        var l_date = new Date(first_day.split("-"));
        l_date.setDate(l_date.getDate() + t_bound);

        // Create scale
        var scale_generic = d3.scaleLinear()
                            .domain([f_date, l_date])
                            .range([800, 0]);


        // Add scales to axis
        var y_axis_generic = d3.axisRight()
                       .scale(scale_generic)
                       .tickSize(10)
                       .tickFormat(d3.timeFormat("%Y-%m"));

        d3.select("#" + groupname).call( y_axis_generic );
      });
}


// var r_bound_g = themap.getBounds()["_northEast"]["lng"]/60;
// var l_bound_g = themap.getBounds()["_southWest"]["lng"]/60;
//
// // Create scale
// var scale_generic = d3.scaleLinear()
//                     .domain([l_bound_g, r_bound_g])
//                     .range([0, 300]);
//
//
// // Add scales to axis
// var x_axis_generic = d3.axisBottom()
//                .scale(scale_generic)
//                .tickValues(d3.range(0, 26, 2));
//
// d3.select("#" + "l_x_g").call( x_axis_generic );


// var zoom = d3.zoom()
//     .translateExtent([[0, 0], [24*60, 6600]])
//     .on("zoom", zoomed);
//
//
// function zoomed(t_bound, b_bound) {
//   // var t_bound = themap.getBounds()["_northEast"]["lat"];
//   // var b_bound = themap.getBounds()["_southWest"]["lat"];
//   console.log(t_bound, b_bound);
//
//   // Create scale
//   var f_date = new Date(first_day.split("-"));
//   f_date.setDate(f_date.getDate() + b_bound);
//
//   var l_date = new Date(first_day.split("-"));
//   l_date.setDate(l_date.getDate() + t_bound);
//
//   console.log(f_date);
//   console.log(l_date);
//
//   // Create scale
//   var scale1 = d3.scaleLinear()
//                 .domain([f_date, l_date])
//                 .range([800, 0]);
//
//   // Add scales to axis
//   var y_axis1 = d3.axisRight()
//                  .scale(scale1)
//                  .tickSize(10)
//                  .tickFormat(d3.timeFormat("%Y-%m"));
//
//   var svg1 = d3.select("#" + groupname)
//   // console.log(svg1)
//   svg1.transition(100).call(y_axis1)
// }
