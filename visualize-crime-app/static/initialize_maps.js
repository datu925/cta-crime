

////////////////////////////////
//Debugging
////////////////////////////////
// var datapoints = [[0,1],
//                   [1,2]]


var bounds = [[0,0], [24,5000]];
var bounds_t = new L.LatLngBounds(new L.LatLng(0, 0), new L.LatLng(6600,24*60));

function init_map(mapname){
  var map = L.map(mapname, {
      // center: bounds_t.getCenter(),
      crs: L.CRS.Simple,
      minZoom: -3,
      maxBounds: bounds_t,
      maxBoundsViscosity: 1.0
  });

  map.fitBounds(bounds)

  return map;
}

var map1 = init_map('map1')
var map2 = init_map('map2')





function update_map(map, station){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         // Typical action to be performed when the document is ready:
          var data = JSON.parse(this.responseText);
          // var response = xhttp.response; //responseText
          console.log(data);

          var heat = L.heatLayer(data,{
             scaleRadius: true,
              // if set to false the heatmap uses the global maximum for colorization
              // if activated: uses the data maximum within the current map boundaries
              //   (there will always be a red spot with useLocalExtremas true)
              useLocalExtrema: true,
              radius: 10,
              blur: 1,
              maxZoom: 17,
              max: 1.0,
              gradient:{ 0.1: 'blue',
                          0.15: 'cyan',
                          0.2: 'lime',
                          0.4: 'yellow',
                          0.5: 'red'}
          }).addTo(map);
      }
  };
  // xhttp.open("GET", "http://0.0.0.0:5000/crimemap?station="+station);
  xhttp.open("GET", "/crimemap?station="+station); //http://0.0.0.0:5000
  // 0.0.0.0:5000/crimemap?station=Howard
  xhttp.send();
}

function clear_map_1(){
map1.eachLayer(function (layer) {
    map1.removeLayer(layer);
});
}
function clear_map_2(){
map2.eachLayer(function (layer) {
    map2.removeLayer(layer);
});
}

var first_day = '2001-01-01';
function get_first_day(){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
         // Typical action to be performed when the document is ready:
          var data = JSON.parse(this.responseText);
          // var response = xhttp.response; //responseText
          console.log(data);
          first_day = data;
      }
  };
  // xhttp.open("GET", "http://0.0.0.0:5000/crimemap?station="+station);
  xhttp.open("GET", "/get_first_day"); //http://0.0.0.0:5000)
  // 0.0.0.0:5000/crimemap?station=Howard
  xhttp.send();
}

get_first_day();
