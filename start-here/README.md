## First Timer?

Welcome! This folder is is designed to give some extra guidance to people who are newer to working with data or are just looking for an easy place to get started. Before you get your feet wet, you should take a scroll through our slack channel #cta-crime on the [ChiHackNight slack channel](https://chihacknight.slack.com/) to see what we've been up to recently!

## Where am I?

You are reading this on our gitlab repository. This section is an intro to exactly what git and gitlab are and why we use them.

Git is a version control system (VCS). This means that whenever we make changes to our project they are all tracked. There are a few big benefits to using git:

1. The entire project's history is tracked and recorded
2. The code and data is hosted in a single accessible place where everyone can access it
3. Multiple people can make changes to the project without conflicting with each other

Gitlab is the website where we store the project. There are multiple websites for storing git projects (like github).

To make changes to the project, you will need to understand some basic things about git. If you want to ignore that for now, you can just download everything as a zip file by clicking on the cloud button.

If you want to be able to make changes you need to have [git installed](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git). To download the repo, just go to the command line and type `git clone https://gitlab.com/datu925/cta-crime.git`. Enter your username and password when prompted and the git repo will be downloaded to the current directory.

## What next?

If you just want to see what sort of data we are working with, go ahead and open up the basic_data.csv file in the start-here folder. If you don't know how to use R or Python, consider using excel or google sheets to explore the data a bit. If you aren't a data person, try looking up some [articles](https://www.chicagotribune.com/news/columnists/wisniewski/ct-biz-cta-thefts-getting-around-20181212-story.html) about crime on the CTA and share them in the slack channel. Think about potentially useful research topics like if crime is increasing in other cities or if there are any techniques police departments are using to decrease crime on public transportation and see what's out there.

## I want to use R or Python to explore the data
Assuming you already have R or python installed on your computer, checkout the basic-analysis files. Inside are some simple analyses that demonstrate some of the effects we've found so far. After you've read through what's there already, see what ideas you have for other analyses using this data.

After you get your feet wet, go and explore the rest of the repo. There's a lot to look at, so read through the README on the main page to get info about what everything is, and what our current tasks are. And if you have any questions, feel free to ask other group members or on the slack channel!