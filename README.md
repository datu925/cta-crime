# Overview

This repository contains data, analysis, and code related to the intersection of crime data and CTA (Chicago Transit Authority) train data. 
Basically, we look at how crime on the CTA has changed over time.

# Quickstart Guide

For the gentlest introduction and some starter data, see the [start_here/](https://gitlab.com/datu925/cta-crime/tree/master/start-here) folder.

1. The enriched data that is most suitable for analysis is available in the `data/out` folder. In particular, crime data is available in `data/out/enriched_cta_crime_data.csv`, and CTA ridership data is available in `data/out/enriched_cta_data.csv`.
1. The original data comes from the city's open data portal: [data.cityofchicago.org](https://data.cityofchicago.org), primarily from:
- https://data.cityofchicago.org/Public-Safety/Crimes-2001-to-present/ijzp-q8t2
- https://data.cityofchicago.org/Transportation/CTA-Ridership-L-Station-Entries-Daily-Totals/5neh-572f

These links contain documentation for the original columns. Here's some documentation for the columns we've added. For more context, see "Preprocessing" below, or look at the scripts themselves in the `scripts/` folder.
- `enriched_cta_data.csv`
    - `line`: indicates what CTA line the ridership is attributed to
    - `line_count`: the number of lines that run through a particular station
    - `rides_split`: indicates the ridership total that should be attributed to that row; `rides / line_count`
- `enriched_cta_crime_data.csv`
    - `closest_stop_id`: the CTA stop_id corresponding to the CTA station that is closest to the crime
    - `["station_name", "map_id", "ada", "red", "blue", "green", "brown", "purple", "pexp", "yellow", "pink", "orange", "line_count"]` - all columns from the CTA stops dataset which indicate what line(s) a station is associated with
    - `crimes_split` - indicates the crime total that should be attributed for that row; 1 / `line_count`

# Preprocessing
To enrich the data, we ran the `process_crime_data.py` and `process_cta_data.py` scripts. Briefly, they:
- filter the crime dataset down to crimes with a CTA-related description
- use the latitude/longitude to find the nearest CTA station for any crime that's been tagged with a CTA-related description
- split crimes and ridership across multiple stations when they can't be naturally disaggregated. For instance, crimes that occurred at Belmont could be associated with the Red, Purple, or Brown lines, so we attribute 1/3 of a crime to each. Similarly, if the CTA reports that 100 riders entered at Belmont on a particular day, we would attribute 33.3 riders to each line.

# Current Findings and Example Analysis

So far:
1. Crime on the CTA has nearly doubled over the last years
1. Pickpocketing is driving that increase
1. The increase is seen most prominently in the Loop
1. Most crimes occur during the afternoon commute
1. Which lines are the more dangerous lines

We also have the beginnings of a website with visualizations and sample graphs: [http://crimevisualizer.43cfivjwz2.us-east-1.elasticbeanstalk.com](http://crimevisualizer.43cfivjwz2.us-east-1.elasticbeanstalk.com)

# Miscellaneous

1. The CTA greatly reduced reporting of fare evasion starting around 2015; we should probably remove these and perhaps narcotics-related crimes as well from the dataset to focus on crimes with victims.

# Work that needs to be done

* whatever you want! This project is still in an exploratory phase
* if you're looking for some more guidance, see our [Issues list](https://gitlab.com/datu925/cta-crime/issues).