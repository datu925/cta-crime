import requests
import os
import pandas as pd


def request_cta_crime_data(output_filename):

    endpoint = "https://data.cityofchicago.org/resource/crimes.json"
    headers = {
        "X-App-Token": os.environ["CHICAGO_OPEN_DATA_KEY"]
    }

    records = []
    count = 0
    retrieved = 1
    while retrieved != 0:
        params = { "$where": "starts_with(location_description, 'CTA')",
                    "$limit": 50000,
                    "$offset": 0 + 50000 * count,
                    }
        req = requests.get(endpoint, headers = headers, params = params)
        records.extend(req.json())
        retrieved = len(req.json())
        count += 1
        if count > 5:
            break
    
    df = pd.DataFrame(records)
    df.to_csv(output_filename, index=False)

if __name__ == "__main__":
    output_filename = "data/chicago_cta_crime_data.csv"
    request_cta_crime_data(output_filename=output_filename)