import pandas as pd
from scipy.spatial.distance import cdist

def process_data(crimes_filename, cta_stops_filename, output_filename):
    df = pd.read_csv(crimes_filename)

    # filters out 'CTA BUS', 'CTA BUS STOP', 'CTA GARAGE / OTHER PROPERTY'
    selected_crimes = [
        'CTA TRAIN', 
        'CTA STATION', 
        'CTA TRACKS - RIGHT OF WAY',
        'CTA PLATFORM',
        'CTA "L" PLATFORM',
        'CTA "L" TRAIN',
        'CTA PROPERTY'
    ]
    remapping = {
        'CTA "L" PLATFORM': 'CTA PLATFORM',
        'CTA "L" TRAIN': 'CTA TRAIN',
    }

    train_crimes = df.loc[df.location_description.isin(selected_crimes)]
    train_crimes.location_description = train_crimes.location_description.replace(to_replace=remapping)

    cta_stops = pd.read_csv(cta_stops_filename, index_col=0)
    cta_stops.columns = map(str.lower, cta_stops.columns)
    cta_stops.rename({"g": "green", "brn": "brown", "p": "purple", "y": "yellow", "pnk": "pink", "o": "orange"}, axis="columns", inplace=True)
    colors = ['red', 'blue', 'green','brown', 'purple', 'yellow', 'pink', 'orange']
    cta_stops[["latitude", "longitude"]] = cta_stops.location.apply(lambda r: pd.Series(r.strip("()").split(",")))
    cta_stops["line_count"] = cta_stops.apply(lambda x: x[colors].sum(), axis=1)
    
    train_crimes["closest_stop_id"] = train_crimes[["latitude", "longitude"]].apply(lambda r: closest_node(r, cta_stops), axis=1)

    merge_columns = ["station_name", "map_id", "ada", "red", "blue", "green", "brown", "purple", "pexp", "yellow", "pink", "orange", "line_count"]
    enriched_train_crimes = train_crimes.merge(cta_stops[merge_columns], how="left", left_on="closest_stop_id", right_index=True)
    enriched_train_crimes["crimes_split"] = 1 / enriched_train_crimes["line_count"]
    tidy = enriched_train_crimes.melt(id_vars=list(set(enriched_train_crimes.columns) - set(colors)), value_vars=colors)
    tidy = tidy.loc[tidy.value].drop(["value", "pexp", "location"], axis=1)
    tidy.rename({"variable": "line"}, axis="columns", inplace=True)
    tidy.to_csv(output_filename)

def closest_node(node, nodes):
    return nodes.iloc[cdist([node], nodes[["latitude", "longitude"]]).argmin()].name

if __name__ == "__main__":
    crimes_filename = "data/chicago_cta_crime_data.csv"
    cta_stops_filename = "data/cta_stops.csv"
    output_filename = "data/out/enriched_cta_crime_data.csv"

    process_data(crimes_filename, cta_stops_filename, output_filename)