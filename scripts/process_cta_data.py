import pandas as pd

def process_cta_data(ridership_filename, stops_filename, output_filename):
    ridership = pd.read_csv(ridership_filename)

    cta_stops = pd.read_csv(stops_filename)
    cta_stops.columns = map(str.lower, cta_stops.columns)
    cta_stops.rename({"g": "green", "brn": "brown", "p": "purple", "y": "yellow", "pnk": "pink", "o": "orange"}, axis="columns", inplace=True)
    colors = ['red', 'blue', 'green','brown', 'purple', 'yellow', 'pink', 'orange']

    stops = cta_stops.groupby('map_id').sum()[colors]
    stops = stops > 0
    stops['line_count'] = stops.apply(lambda x: sum(x), axis=1)

    # TODO: figure out how to treat some of the stations that no longer exist
    merged = ridership.merge(stops, how="inner", left_on="station_id", right_index=True)
    merged["rides_split"] = merged['rides'] / merged["line_count"]
    tidy = merged.melt(id_vars=["rides_split", "date", "station_id", "stationname"], value_vars=colors)
    tidy = tidy.loc[tidy.value].drop("value", axis=1)
    tidy.rename({"variable": "line"}, axis="columns", inplace=True)
    tidy.to_csv(output_filename)

if __name__ == "__main__":
    ridership_filename = "data/ridership_data.csv"
    stops_filename = "data/cta_stops.csv"
    output_filename = "data/out/enriched_cta_data.csv"

    process_cta_data(ridership_filename, stops_filename, output_filename)